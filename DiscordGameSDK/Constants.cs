﻿using System;

namespace Discord
{
    static class Constants
    {
        public const string DllNameX86 = "discord_game_sdk_x86";

        public const string DllNameX64 = "discord_game_sdk_x64";

#if WIN32

        public const string DllName = DllNameX86;
#elif WIN64

        public const string DllName = DllNameX64;

#elif !ANYCPU

#error One of 'WIN32', 'WIN64' or 'ANYCPU' must be specified

#endif

    }
}
