# discord-game-sdk-csharp

Visual studio solution with C# assembly to use with DiscordGameSDK DLLs (for now, Windows-only; DLLs provided).

DLLs are newest by Mar 13, 2021 (they do not have embedded version number but download link says it's 2.5.6).

Dlls belong to Discord and newest versions are available on Discord Developer Portal [here](https://discord.com/developers/docs/game-sdk/sdk-starter-guide).

## About license
Provided DLLs are from official [Discord Developer Portal](https://discord.com/developers) and are not considerered FOSS, thus repository has no LICENSE file. Note that if this way of distribution is not in line with Discord's Terms of Service, either binaries or the repo itself will be deleted.
